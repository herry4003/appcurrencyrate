package com.app.currencyrate

object Constants {

     const val BASE_URL ="https://revolut.duckdns.org/"
     const val DEFAULT_BASE_RATE="EUR"
     const val LEFT_VIEW_SPACE=110
}