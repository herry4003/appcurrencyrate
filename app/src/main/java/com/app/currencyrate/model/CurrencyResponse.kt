package com.app.currencyrate.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

class CurrencyResponse {

    @SerializedName("base")
    var base: String=""

    @SerializedName("date")
    var date: String=""

    @SerializedName("rates")
    var ratesMap: JsonObject = JsonObject()
}