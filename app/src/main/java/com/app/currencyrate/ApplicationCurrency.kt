package com.app.currencyrate

import android.app.Application


class ApplicationCurrency:Application(){


    companion object{
        private var mAppInstance:ApplicationCurrency?=null

        fun getAppInstance():ApplicationCurrency{

            if(mAppInstance==null){

                throw NullPointerException("App not created!!")
            }

            return mAppInstance!!

        }

    }

    override fun onCreate() {
        super.onCreate()
        mAppInstance=this
    }




}