package com.app.currencyrate.activities

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.app.currencyrate.ApplicationCurrency
import com.app.currencyrate.R
import com.app.currencyrate.controllers.RateApiController
import com.app.currencyrate.controllers.ViewController
import com.app.currencyrate.factory.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_main.*

class RateActivity : AppCompatActivity() {

    private var mRateApiController:RateApiController?=null
    private var mViewController:ViewController?=null
    private var mObserverBaseRateName:Observer<String>?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ViewModelProviderFactory.instance.initModels(ApplicationCurrency.getAppInstance())
        ViewModelProviderFactory.instance.setLifeCycleOwner(this)
        initController()
    }


    private fun initController(){
        mViewController= ViewController(linearLayout_RateList,this)
        mViewController?.setScrollView(scrollView_ratelist)
        mViewController?.mConstrainLayout=constrainLayout_main
        lifecycle.addObserver(mViewController!!)
        mRateApiController= RateApiController(ll_loading_view)
        lifecycle.addObserver(mRateApiController!!)

        mObserverBaseRateName= Observer {
            if(it!=null){
                txt_base_rate.text=it
            }
        }

        ViewModelProviderFactory.instance.observerBaseRateViewModel(mObserverBaseRateName!!)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        when {
            newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE -> {
                restoreScrollState()
                mViewController?.updateEditextMaxWidth(false)
            }
            newConfig.orientation == Configuration.ORIENTATION_PORTRAIT -> {
                restoreScrollState()
                mViewController?.updateEditextMaxWidth(true)
            }
        }
    }

    private fun restoreScrollState(){
        if(mViewController?.getScrollState()!=null){
            scrollView_ratelist.onRestoreInstanceState(mViewController?.getScrollState())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mObserverBaseRateName!=null){
            ViewModelProviderFactory.instance.removeBaseRateViewModel(mObserverBaseRateName!!)
        }
        if(mRateApiController!=null){
            lifecycle.removeObserver(mRateApiController!!)
        }
    }
}
