package com.app.currencyrate.factory

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.currencyrate.model.CurrencyRowItem
import com.app.currencyrate.viewmodel.CurrencyMapModel
import com.app.currencyrate.viewmodel.CurrencyViewModel

class ViewModelProviderFactory {

    private var mCurrencyViewModel:CurrencyViewModel?=null
    private var mCurrencyMapModel: CurrencyMapModel?=null

    private var mLifeCycleOwner:LifecycleOwner?=null

    private object Holder {
        val INSTANCE = ViewModelProviderFactory()
    }


    companion object{
        val instance:ViewModelProviderFactory by lazy { Holder.INSTANCE }
    }


    fun setLifeCycleOwner(lifeCycleOwner:LifecycleOwner){
        mLifeCycleOwner=lifeCycleOwner
    }

    fun initModels(application: Application){

        mCurrencyViewModel=ViewModelProvider.AndroidViewModelFactory(application).create(CurrencyViewModel::class.java)
        mCurrencyViewModel?.initModel()

        mCurrencyMapModel=ViewModelProvider.AndroidViewModelFactory(application).create(CurrencyMapModel::class.java)
        mCurrencyMapModel?.initModel()

    }

    fun getCurrencyViewModel():CurrencyViewModel?{
        return mCurrencyViewModel
    }

    fun getCurrencyMapModel():CurrencyMapModel?{
        return mCurrencyMapModel
    }

    fun observerCurrencyListViewModel(observerList:Observer<ArrayList<CurrencyRowItem>>){

        mCurrencyViewModel?.getCurrentListViewModel()?.observe(mLifeCycleOwner!!,observerList)
    }

    fun removeCurrentListViewModel(observerList:Observer<ArrayList<CurrencyRowItem>>){

        mCurrencyViewModel?.getCurrentListViewModel()?.removeObserver(observerList)
    }

    fun observerCurrencyMapViewModel(observerList:Observer<MutableMap<String,Double>>){

        mCurrencyMapModel?.getRateLiveModel()?.observe(mLifeCycleOwner!!,observerList)
    }

    fun removeCurrencyMapViewModel(observerList:Observer<MutableMap<String,Double>>){

        mCurrencyMapModel?.getRateLiveModel()?.removeObserver(observerList)
    }

    fun observerBaseRateViewModel(observerBaseRate:Observer<String>){
        mCurrencyMapModel?.getBaseRateLiveModel()?.observe(mLifeCycleOwner!!,observerBaseRate)
    }

    fun removeBaseRateViewModel(observerBaseRate:Observer<String>){
        mCurrencyMapModel?.getBaseRateLiveModel()?.removeObserver(observerBaseRate)
    }

}