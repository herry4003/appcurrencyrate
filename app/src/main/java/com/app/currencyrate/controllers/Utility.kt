package com.app.currencyrate.controllers

import android.app.Activity
import android.content.res.Resources
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup

const val KEYBOARD_VISIBLE_THRESHOLD_DP=100

fun Activity.isKeyboardOpened(): Int {
    val r = Rect()

    val activityRoot = getActivityRoot()


    activityRoot.getWindowVisibleDisplayFrame(r)

    return  activityRoot.rootView.height - r.height()
}

fun Activity.getDeviceHeight():Int{
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun Activity.getDeviceWidth():Int{
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun Activity.getActivityRoot(): View {
    return (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
}

object Utility {



    fun dip(value: Int): Int {
        return (value * Resources.getSystem().displayMetrics.density).toInt()
    }

}