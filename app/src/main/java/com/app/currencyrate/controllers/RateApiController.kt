package com.app.currencyrate.controllers

import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.app.currencyrate.factory.ViewModelProviderFactory
import com.app.currencyrate.model.CurrencyResponse
import com.app.currencyrate.model.CurrencyRowItem
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class RateApiController(loadingView:LinearLayout):LifecycleObserver{

    private var mObservable:Observable<Long>?=null
    private val mDisposable = CompositeDisposable()
    private var mLoadingView:LinearLayout?=loadingView

      @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
      fun onCreate(){
          MapUtils.initMaps()
          initApiObserver()
      }

     private fun initApiObserver(){
         mLoadingView?.visibility= View.VISIBLE
         val rateApiInterface=RateApiClient.getRateApiInterface()
         mObservable= Observable.interval(1, TimeUnit.SECONDS)

         val compose=mObservable?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.map {tick->
             val currency=ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrecyViewModel()?.value//Call APi with currency  base
             rateApiInterface!!.getCurrencyRate(currency!!.baseCurrency).subscribeOn(Schedulers.io()).
                 observeOn(AndroidSchedulers.mainThread()).map {
                 reseponse->reseponse
             }
         }?.retry()?.subscribe({jsonObserver->jsonObserver.observeOn(AndroidSchedulers.mainThread()).subscribe ({
             jsonResponse-> if(jsonResponse!=null){
                                parseResponse(jsonResponse)
                                hideProgressbar()

                            }
         },{
             networkError->Log.e("API","Error Network ${networkError}")
             hideProgressbar()
         })
         },{
              excelption->Log.e("API","Error ${excelption}")

         })

         mDisposable.add(compose!!)

     }

    private fun hideProgressbar(){
        if(mLoadingView?.visibility==View.VISIBLE){
            mLoadingView?.visibility= View.GONE
        }
    }

    private fun parseResponse(jsonObject: JsonObject){
        val gson= Gson()

        val currencyResponse=gson.fromJson(jsonObject.toString(),CurrencyResponse::class.java)
        val countryCodes=currencyResponse.ratesMap.keySet()

        val currencyMap=HashMap<String,Double>()

        val arrayCurrencyList=ArrayList<CurrencyRowItem>()

        countryCodes.forEachIndexed { _, key->
            currencyMap[key]=currencyResponse.ratesMap.get(key).asDouble
            val currencyRowItem=CurrencyRowItem(key)
            arrayCurrencyList.add(currencyRowItem)
        }
        val currencyRowItem=CurrencyRowItem(currencyResponse.base)
        arrayCurrencyList.add(currencyRowItem)

        currencyMap[currencyResponse.base]=1.0
        ViewModelProviderFactory.instance.getCurrencyMapModel()?.setBaseRate(currencyResponse.base)
        ViewModelProviderFactory.instance.getCurrencyMapModel()?.setRateLiveModel(currencyMap)

        if(ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrentListViewModel()?.value==null){
            ViewModelProviderFactory.instance.getCurrencyViewModel()?.setCurrentListViewModel(arrayCurrencyList)
        }


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy(){
        if(!mDisposable.isDisposed){
            mDisposable.clear()
        }
    }

}