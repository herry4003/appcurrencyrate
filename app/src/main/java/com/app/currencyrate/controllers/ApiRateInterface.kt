package com.app.currencyrate.controllers

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRateInterface {

    @GET("/latest?")
    fun getCurrencyRate(@Query("base") currencyBase: String): Observable<JsonObject>
}