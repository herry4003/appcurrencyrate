package com.app.currencyrate.controllers

import com.app.currencyrate.R


object MapUtils {

    val mMapCurrencyName=HashMap<String,String>()
    val mMapFlags=HashMap<String,Int>()

     fun initMaps(){
        with(mMapCurrencyName) {
           put("EUR","Euro")
           put("AUD","Australian Dollar")
           put("BGN","Bulgarian Lev")
           put("BRL","Brazilian Real")
           put("CAD","Canadian Dollar")
           put("CHF","Swiss Franc")
           put("CNY","Chinese Yuan Renminbi")
           put("CZK","Czech Koruna")
           put("DKK","Danish Krone")
           put("GBP","Pound Sterling")
           put("HKD","Hong Kong Dollar")
           put("HRK","Croatian Kuna")
           put("HUF","Hungarian Forint")
           put("IDR","Indonesian Rupiah")
           put("ILS","Israeli New Shekel")
           put("INR","Indian Rupee")
           put("ISK","Icelandic Krona")
           put("JPY","Japanese Yen")
           put("KRW","South Korean Won")
           put("MXN","Mexican Peso")
           put("MYR","Malaysian Ringgit")
           put("NOK","Norwegian Krone")
           put("NZD","New Zealand Dollar")
           put("PHP","Philippine Peso")
           put("PLN","Polish Zloty")
           put("RON","Romanian Leu")
           put("RUB","Russian Ruble")
           put("SEK","Swedish Krona")
           put("SGD","Singapore Dollar")
           put("THB","Thai Baht")
           put("TRY","Turkish Lira")
           put("USD","US Dollar")
           put("ZAR","South African Rand")
        }

        with(mMapFlags) {
           put("EUR", R.drawable.ic_list_eu)
           put("AUD", R.drawable.ic_list_au)
           put("BGN", R.drawable.ic_list_bg)
           put("BRL", R.drawable.ic_list_br)
           put("CAD", R.drawable.ic_list_ca)
           put("CHF", R.drawable.ic_list_fr)
           put("CNY", R.drawable.ic_list_cn)
           put("CZK", R.drawable.ic_list_cz)
           put("DKK", R.drawable.ic_list_dk)
           put("GBP", R.drawable.ic_list_uk)
           put("HKD", R.drawable.ic_list_hk)
           put("HRK", R.drawable.ic_list_hr)
           put("HUF", R.drawable.ic_list_hu)
           put("IDR", R.drawable.ic_list_id)
           put("ILS", R.drawable.ic_list_il)
           put("INR", R.drawable.ic_list_in)
           put("ISK", R.drawable.ic_list_is)
           put("JPY", R.drawable.ic_list_jp)
           put("KRW", R.drawable.ic_list_kr)
           put("MXN", R.drawable.ic_list_mx)
           put("MYR", R.drawable.ic_list_my)
           put("NOK", R.drawable.ic_list_no)
           put("NZD", R.drawable.ic_list_nz)
           put("PHP", R.drawable.ic_list_ph)
           put("PLN", R.drawable.ic_list_pl)
           put("RON", R.drawable.ic_list_ro)
           put("RUB", R.drawable.ic_list_ru)
           put("SEK", R.drawable.ic_list_se)
           put("SGD", R.drawable.ic_list_sg)
           put("THB", R.drawable.ic_list_th)
           put("TRY", R.drawable.ic_list_tr)
           put("USD", R.drawable.ic_list_us)
           put("ZAR", R.drawable.ic_list_za)
        }

    }
}