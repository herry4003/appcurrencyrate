package com.app.currencyrate.controllers

import com.app.currencyrate.BuildConfig
import com.app.currencyrate.Constants.BASE_URL
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RateApiClient {

    private  var mRetrofit:Retrofit?=null
    private  var mApiInterface: ApiRateInterface?=null

    private val retrofitClient:Retrofit?
                get() {
                    if(mRetrofit==null){
                        val httpLogger= HttpLoggingInterceptor()
                        httpLogger.apply {
                            httpLogger.level = HttpLoggingInterceptor.Level.BODY
                        }

                        val okHttpClient = OkHttpClient.Builder()
                            .readTimeout(60, TimeUnit.SECONDS)
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)

                         if(BuildConfig.DEBUG){
                             okHttpClient.addInterceptor(httpLogger)
                         }

                        mRetrofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(okHttpClient.build())
                            .build()
                    }

                    return mRetrofit
                }


    fun getRateApiInterface():ApiRateInterface?{
        if(mApiInterface==null){
            mApiInterface=retrofitClient?.create(ApiRateInterface::class.java)
        }
        return mApiInterface
    }


}