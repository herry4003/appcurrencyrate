package com.app.currencyrate.controllers

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.graphics.Rect
import android.os.Build
import android.os.Parcelable
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import com.app.currencyrate.R
import com.app.currencyrate.Constants.LEFT_VIEW_SPACE
import com.app.currencyrate.factory.ViewModelProviderFactory
import com.app.currencyrate.model.CurrencyRowItem
import com.app.currencyrate.widget.NestedScrollSaveView
import com.jakewharton.rxbinding3.widget.TextViewTextChangeEvent
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.mikhaellopez.circularimageview.CircularImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


class ViewController(var mainLayout: LinearLayout, var context: Context) : LifecycleObserver {

    private var mListObserver: Observer<ArrayList<CurrencyRowItem>>? = null
    private var mArrayListInput = ArrayList<EditText>()
    private var mArrayTxtSymbol = ArrayList<TextView>()
    private var mArrayTxtCurrency = ArrayList<TextView>()
    private var mArrayImageView = ArrayList<CircularImageView>()
    private var mArrayRowRoot = ArrayList<ConstraintLayout>()
    private val mDisposable = CompositeDisposable()
    private val mDecimalFormat = DecimalFormat("#.####")
    private var mObserverCurrencyMap: Observer<MutableMap<String, Double>>? = null
    private var mEditext: EditText? = null
    private var mScrollView: NestedScrollSaveView? = null
    private var mNestedScrollState: Parcelable? = null
    var mConstrainLayout:ConstraintLayout?=null
    private var mMeasureSpec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
    private var mMeasurePortWidth=-1
    private var mMinWidthPort=0
    private var mMinWidthLand=0


    fun getScrollState(): Parcelable? {
        return mNestedScrollState
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {

        mListObserver = Observer {
            if (it != null && it.isNotEmpty()) {
                insertViews()
            }
        }

        ViewModelProviderFactory.instance.observerCurrencyListViewModel(mListObserver!!)

        mObserverCurrencyMap = Observer {
            var currencyEnter = ViewModelProviderFactory.instance.getCurrencyViewModel()?.getUserEnterCurrency()
            if (currencyEnter!!.isNotEmpty()) {
                updateValues(currencyEnter.toDouble())
            }
        }

        ViewModelProviderFactory.instance.observerCurrencyMapViewModel(mObserverCurrencyMap!!)


    }

    fun setScrollView(scrollView: NestedScrollSaveView) {
        mScrollView = scrollView

        mScrollView?.setOnScrollChangeListener { nestedScrollView: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->

            if (mEditext != null) {
                val visiblePercentage = getVisiblePercent(mEditext!!)
                if (visiblePercentage < 10) {
                    closeEditxt()
                }
            }
            mNestedScrollState = mScrollView?.onSaveInstanceState()
        }

    }

    private fun closeEditxt(){
        hideSoftKeyboard(mEditext!!)
        mEditext?.clearFocus()
        mEditext?.isCursorVisible = false
        mEditext = null
    }

    private fun hideSoftKeyboard(editText: EditText) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }

    private fun getVisiblePercent(v: View): Int {
        return if (v.isShown) {
            val r = Rect()
            val isVisible = v.getGlobalVisibleRect(r)
            if (isVisible) {
                val sVisible = r.width() * r.height()
                val sTotal = (v.width * v.height).toDouble()
                (100 * sVisible / sTotal).toInt()
            } else {
                -1
            }
        } else {
            -1
        }
    }



    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        if (mListObserver != null) {
            ViewModelProviderFactory.instance.removeCurrentListViewModel(mListObserver!!)
        }

        if (mObserverCurrencyMap != null) {
            ViewModelProviderFactory.instance.removeCurrencyMapViewModel(mObserverCurrencyMap!!)
        }

        if (!mDisposable.isDisposed) {
            mDisposable.clear()
        }
    }

    private fun insertViews() {
        if (mainLayout.childCount == 0) {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            //Insert Row
            val listView = ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrentListViewModel()?.value
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            listView?.forEachIndexed { _, currencyRowItem ->

                val rowView = layoutInflater.inflate(R.layout.layout_raw_currency_raw, mainLayout, false)
                val txtCurrencyCode = rowView.findViewById<TextView>(R.id.txt_currency_symbol)
                val txtCurrencyName = rowView.findViewById<TextView>(R.id.txt_currency_name)
                val edittxtInputCurrency = rowView.findViewById<EditText>(R.id.editxt_currency_value)
                EditextObserver(edittxtInputCurrency)
                val imageFlag = rowView.findViewById<CircularImageView>(R.id.roundCardView)
                imageFlag.setImageResource(MapUtils.mMapFlags[currencyRowItem.currencyCode]!!)
                txtCurrencyName.text = MapUtils.mMapCurrencyName[currencyRowItem.currencyCode]
                txtCurrencyCode.text = currencyRowItem.currencyCode
                edittxtInputCurrency.tag = currencyRowItem.currencyCode
                edittxtInputCurrency.onFocusChangeListener = mViewFocusEvent
                rowView.setOnClickListener(mViewClickListener)
                mArrayRowRoot.add(rowView as ConstraintLayout)
                mArrayTxtSymbol.add(txtCurrencyCode)
                mArrayTxtCurrency.add(txtCurrencyName)
                mArrayListInput.add(edittxtInputCurrency)
                mArrayImageView.add(imageFlag)

                mainLayout.addView(rowView, layoutParams)

                txtCurrencyName.measure(mMeasureSpec,mMeasureSpec)
                if(mMeasurePortWidth<txtCurrencyName.measuredWidth){
                    mMeasurePortWidth=txtCurrencyName.measuredWidth

                    //Find width for the
                    if(context.resources.configuration.orientation== Configuration.ORIENTATION_LANDSCAPE){
                        mMinWidthLand =(context as Activity).getDeviceWidth()-mMeasurePortWidth
                        mMinWidthPort =(context as Activity).getDeviceHeight()-mMeasurePortWidth

                    }else{
                        mMinWidthPort=(context as Activity).getDeviceWidth()-mMeasurePortWidth
                        mMinWidthLand=(context as Activity).getDeviceHeight()-mMeasurePortWidth
                    }

                    mMinWidthPort-=Utility.dip(LEFT_VIEW_SPACE)
                    mMinWidthLand-=Utility.dip(LEFT_VIEW_SPACE)
                }

            }
            if(context.resources.configuration.orientation== Configuration.ORIENTATION_LANDSCAPE){
                updateEditextMaxWidth(false)
            }else{
                updateEditextMaxWidth(true)
            }

        }

    }

    fun updateEditextMaxWidth(isPort: Boolean){
        mArrayListInput.forEach {editText->
            if(isPort){
                if(mMinWidthPort>0){
                    editText.maxWidth=mMinWidthPort
                }
            }else{
                if(mMinWidthLand>0){
                    editText.maxWidth=mMinWidthLand
                }
            }

        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private var mViewClickListener = View.OnClickListener { view ->

        try{
            val index=mainLayout.indexOfChild(view)
            if(index!=0){
                TransitionManager.beginDelayedTransition(mConstrainLayout!!)
                mainLayout.removeViewAt(index)
                mainLayout.addView(view,0)
                mScrollView?.smoothScrollTo(0,0)
                mScrollView?.postDelayed({
                    val editText=view.findViewById<EditText>(R.id.editxt_currency_value)
                    editText.isFocusable=true
                    editText.requestFocus()
                    editText.setSelection(editText.text.length)
                    showSoftKeyboard(editText)
                },150)

            }
        }catch (exception:Exception){
            Log.e("Exc","Unsupported operation")
        }

    }


    private fun showSoftKeyboard(editext:EditText){
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editext,0)
    }
    private var mViewFocusEvent = View.OnFocusChangeListener { editext, focused ->
        if (focused) {
            mEditext = editext as EditText
            mEditext?.isCursorVisible = true
            val tag = editext.tag as String
            val currency = ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrecyViewModel()?.value
            currency!!.baseCurrency = tag
            ViewModelProviderFactory.instance.getCurrencyViewModel()?.setCurrecyViewModel(currency)
            val inputBox = editext as EditText
            if (inputBox.text.isNotEmpty()) {
                ViewModelProviderFactory.instance.getCurrencyViewModel()?.setUserEnterCurrency(inputBox.text.toString())
            }
            EditextObserver(editext)
        }
    }


    inner class EditextObserver(editext: EditText) {
        init {
            val disposable =
                editext.textChangeEvents().subscribeOn(Schedulers.io()).debounce(100, TimeUnit.MILLISECONDS)
                    .map { textChange: TextViewTextChangeEvent ->
                        if (textChange.view.isFocused) {
                            if (textChange.text.toString().isNotEmpty()) {
                                textChange.text.toString()
                            } else {
                                "-1"
                            }
                        } else {
                            ""
                        }
                    }.observeOn(AndroidSchedulers.mainThread()).subscribe { it ->
                    if (it.isNotEmpty() && it != "-1") {
                        updateValues(it.toDouble())
                    } else if (it.isNotEmpty() && it == "-1") {
                        //Clear All Currency
                        ViewModelProviderFactory.instance.getCurrencyViewModel()?.setUserEnterCurrency("")
                        clearAllInputs()
                    }
                }

            mDisposable.add(disposable)
        }

    }

    private fun clearAllInputs() {

        mArrayListInput.forEachIndexed { _, editText ->
            if (!editText.isFocused) {
                editText.setText("")
            }
        }
    }

    private fun updateValues(userEnter: Double) {
        val baseRate = ViewModelProviderFactory.instance.getCurrencyMapModel()?.getBaseRate()
        val currency = ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrecyViewModel()?.value
        if (currency!!.baseCurrency == baseRate) {
            ViewModelProviderFactory.instance.getCurrencyViewModel()?.setUserEnterCurrency(userEnter.toString())
            val mapData = ViewModelProviderFactory.instance.getCurrencyMapModel()?.getRateLiveModel()?.value
            Log.i("Update", "Update list rate")
            mArrayListInput?.forEachIndexed { _, editText ->
                val tag = editText.tag as String
                if (tag != baseRate) {
                    val mRateValue = mapData!!.getValue(tag) * userEnter
                    editText.setText(mDecimalFormat.format(mRateValue))
                }
            }

        }else{
            //Use Custom rate converter, Base on current base rate
            val mHashData = ViewModelProviderFactory.instance.getCurrencyMapModel()?.getRateLiveModel()?.value as MutableMap
            val currencyManual = ViewModelProviderFactory.instance.getCurrencyViewModel()?.getCurrecyViewModel()?.value!!
            /**
             * User Enter Currency code
             */
            val baseRateValue=mHashData.getValue(currencyManual.baseCurrency)
            /**
             * Existing Rate list Base Currency Code
             */
            ViewModelProviderFactory.instance.getCurrencyViewModel()?.setUserEnterCurrency(userEnter.toString())
             //Update list by existing base rate
            val userNewValue=userEnter.div(baseRateValue)
            val userEnterValue=mDecimalFormat.format(userNewValue).toDouble()
            updateList(currencyManual.baseCurrency,userEnterValue,mHashData)

        }
    }

    private fun updateList(currentTag:String,enterValue:Double,mapData:MutableMap<String,Double>){

        mArrayListInput.forEachIndexed { _, editText ->
            val tag = editText.tag as String
            if (tag != currentTag) {
                val mRateValue = mapData.getValue(tag) * enterValue
                editText.setText(mDecimalFormat.format(mRateValue))
            }
        }
    }


}