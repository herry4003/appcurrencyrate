package com.app.currencyrate.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.app.currencyrate.Constants


class CurrencyMapModel(application: Application) :AndroidViewModel(application){

     private var mRateLiveModel:MutableLiveData<MutableMap<String,Double>>?=null
     private var mBaseRateLiveData:MutableLiveData<String>?=null

     fun initModel(){
         if(mRateLiveModel==null){
             mRateLiveModel= MutableLiveData()
         }

         if(mBaseRateLiveData==null){
             mBaseRateLiveData= MutableLiveData()
             mBaseRateLiveData?.value=Constants.DEFAULT_BASE_RATE
         }
     }

    fun setRateLiveModel(map:MutableMap<String,Double>){

        mRateLiveModel?.value=map
    }

    fun getRateLiveModel():MutableLiveData<MutableMap<String,Double>>?{
        return mRateLiveModel
    }

    fun setBaseRate(baseRate:String){
        mBaseRateLiveData?.value=baseRate
    }

    fun getBaseRate():String?{
        return mBaseRateLiveData?.value
    }

    fun getBaseRateLiveModel():MutableLiveData<String>?{
        return mBaseRateLiveData
    }


}