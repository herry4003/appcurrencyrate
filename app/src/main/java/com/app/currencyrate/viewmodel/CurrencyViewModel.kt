package com.app.currencyrate.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.app.currencyrate.Constants
import com.app.currencyrate.model.Currency
import com.app.currencyrate.model.CurrencyRowItem

class CurrencyViewModel(application: Application) :AndroidViewModel(application){

    private var mLiveCurrencyViewModel:MutableLiveData<Currency>?=null

    private var mListCurrenyViewModel:MutableLiveData<ArrayList<CurrencyRowItem>>?=null

    private var mUserEnterViewModel:MutableLiveData<String>?=null

    fun initModel(){
        if(mLiveCurrencyViewModel==null){
            mLiveCurrencyViewModel= MutableLiveData()
            mLiveCurrencyViewModel?.value= Currency(Constants.DEFAULT_BASE_RATE)
        }

        if(mListCurrenyViewModel==null){
            mListCurrenyViewModel= MutableLiveData()

        }

        if(mUserEnterViewModel==null){
            mUserEnterViewModel= MutableLiveData()
            mUserEnterViewModel?.value=""
        }

    }


    fun getCurrecyViewModel():MutableLiveData<Currency>?{
        return mLiveCurrencyViewModel
    }

    fun setCurrecyViewModel(currency: Currency){
        mLiveCurrencyViewModel?.value=currency
    }

    fun setCurrentListViewModel(list:ArrayList<CurrencyRowItem>){
        mListCurrenyViewModel?.value=list
    }

    fun getCurrentListViewModel():MutableLiveData<ArrayList<CurrencyRowItem>>?{
        return mListCurrenyViewModel
    }

    fun setUserEnterCurrency(currency: String){
        mUserEnterViewModel?.value=currency
    }

    fun getUserEnterCurrency():String{
        return mUserEnterViewModel?.value!!
    }


}